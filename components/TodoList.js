import React from 'react';

const TodoRow = React.createClass({
    render: function () {
        return (
        <li>
            <span>{this.props.text}</span> &nbsp;
            <span>{this.props.status ? 'Active' : 'Done'}</span> &nbsp;
            <input type='button' value='Remove' />
            </li>
        );
    }
});

const genTodoRow = function(todo) {
    return (
            <TodoRow key={todo.id} text={todo.text} status={todo.status} />
    );
};

export default React.createClass({
    render: function() {
        return (
            <div>
            <h3>Todo List</h3>
                <ul>
                {this.props.todos.map(genTodoRow)}
                </ul>
                </div>
        );
    }
});
