import React from 'react';
//import { createStore } from 'redux';
//import { Provider } from 'react-redux';
import TodoList from './TodoList.js';
import NewTodo from './NewTodo.js';
import TodoFooter from './TodoFooter.js';

// yasnippets
/*
const todos = [
  {id: 1, text: 'Buy milk', active: true},
  {id: 2, text: 'Do laundryyyyy', active: false},
  {id: 3, text: 'Read a book', active: false},
  {id: 4, text: 'write Add handler', active: true},
  {id: 5, text: 'write footer handler', active: true}
]; */

let todoId = 0;

export default React.createClass({
  getInitialState: function() {
    return {todos: []};
  },
  updateTodos: function(newTodo) {
    var allTodos = this.state.todos.concat({id: todoId++,
                                          text: newTodo,
                                          status: true
                                         });
    this.setState({todos: allTodos});
  },
  render: function() {
    return (
        <div>
        <NewTodo addTodo={ this.updateTodos } />
        <TodoList todos={ this.state.todos } />
        <TodoFooter />
        </div>
    );
  }
});
