import React from 'react';

export default React.createClass({
  getInitialState: function() {
    return {
      todo: ''
    };
  },
  onChange: function(e) {
    this.setState({ todo: e.target.value });
  },
  onClick: function(e) {
    e.preventDefault();
    this.props.addTodo(this.state.todo);
    this.setState({ todo: e.target.value });
    return;
  },
  render: function() {
    return (
        <div>
        <span>New Todo : </span>&nbsp;
        <input type='text' placeholder='enter todo' onChange={this.onChange} value={this.state.todo} />&nbsp;
        <input type='button' value='Add' onClick={this.onClick} />
        </div>
    );
  }
});
